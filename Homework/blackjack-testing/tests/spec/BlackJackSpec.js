
  describe('calcPoints Test', function() {
    describe('test four hands of cards ', function() {
      it('calcPoints total score', function() {
        const hand1 = [
          {
            suit: 'spades',
            val: 10,
            displayVal: '10',
          },
          {
            suit: 'spades',
            val: 9,
            displayVal: '9',
          }
        ];
        
        const hand2 = [
          {
          suit: 'spade',
          val: 11,
          displayVal: 'Ace',
          },
          {
            suit: 'spades',
            val: 6,
            displayVal: '6',
          }
        ];
        
        const hand3 = [
          {
            suit: 'spades',
            val: 10,
            displayVal: '10',
          },
          {
            suit: 'spades',
            val: 7,
            displayVal: '7',
          }

        ];

        const hand4 = [
          {
            suit: 'spades',
            val: 2,
            displayVal: '2',
          },
          {
            suit: 'spades',
            val: 4,
            displayVal: '4',
          },
          {
            suit: 'spades',
            val: 2,
            displayVal: '2',
          },
          {
            suit: 'spades',
            val: 5,
            displayVal: '5',
          }
        ];
        
        expect(calcPoints(hand1).total).toEqual(19);
        expect(calcPoints(hand2).total).toEqual(17);
        expect(calcPoints(hand3).total).toEqual(17);
        expect(calcPoints(hand4).total).toEqual(13);

        })
    })
})

