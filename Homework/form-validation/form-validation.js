/* Validates an individual input on form submitEvent
/* @param {HTMLElement} inputEl
/* @param {Event} sumbitEvent
*/

// validate name for three characters or more
const validateName = function(inputEl, submitEvent) {
    // const target = submitEvent.target;
    //var constraint = new RegExp(constraints, "");

    const errorEl = inputEl.parentElement.querySelector('.error');
    
    if (inputEl.value === '' || inputEl.value.length < 3) {
        submitEvent.preventDefault();
        console.log('Bad input');

        const labelEL  = inputEl.parentElement.querySelector('label');

        errorEl.innerHTML = `${labelEL.innerText} must be at least 3 characters long.`;
        inputEl.parentElement.classList.add('invalid');

    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = ``;

    };
};

// validate email
const validateEmail = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');

    //const constraints = {
        //name : ['/\w{3,}/', 'Name must have 3 characters or more'],
    const emailRegEx = /\w+@\w+\.\w+/i;//, 'Email must have valid format: e.g. example@email.com']
   // };

    if (inputEl.value === '' || !inputEl.value.match(emailRegEx)) {   
        submitEvent.preventDefault(); 
        const error = 'Bad input';  
        console.log(error);

        const labelEL  = inputEl.parentElement.querySelector('label');
        errorEl.innerHTML = `Valid ${labelEL.innerText} required.`;
        inputEl.parentElement.classList.add('invalid');
    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = '';
    };

};

// //TODO check if person wants to chat about jobs
// const validateJob = function(inputEl, submitEvent) {
//     const errorEl = inputEl.parentElement.querySelector('.error');

//     if (inputEl.value === '' || !inputEl.value.match(emailRegEx)) {   
//         submitEvent.preventDefault(); 

//         const labelEL  = inputEl.parentElement.querySelector('label');
//         errorEl.innerHTML = `Valid ${labelEL.innerText} required.`;
//         inputEl.parentElement.classList.add('invalid');
//     } else {
//         inputEl.parentElement.classList.remove('invalid');
//         errorEl.innerHTML = '';
//     };
// };

// //TODO check if person wants to chat about code
// const validateTalk = function(inputEl, submitEvent) {
//     //check if person wants to chat about code
//     const errorEl = inputEl.parentElement.querySelector('.error');

//     if (inputEl.value === '' || inputEl.value.length < 10) {   
//         submitEvent.preventDefault(); 
//         const error = 'Bad input';  
//         console.log(error);

//         const labelEL  = inputEl.parentElement.querySelector('label');
//         errorEl.innerHTML = `${labelEL.innerText}  should be at least 10 characters.`;
//         inputEl.parentElement.classList.add('invalid');
//     } else {
//         inputEl.parentElement.classList.remove('invalid');
//         errorEl.innerHTML = '';
//     };
// };

// check message has 10 characters
const validateMessage = function(inputEl, submitEvent) {
    const errorEl = inputEl.parentElement.querySelector('.error');

    if (inputEl.value === '' || inputEl.value.length < 10) {   
        submitEvent.preventDefault(); 
        const error = 'Bad input';  
        console.log(error);

        const labelEL  = inputEl.parentElement.querySelector('label');
        errorEl.innerHTML = `${labelEL.innerText}  should be at least 10 characters.`;
        inputEl.parentElement.classList.add('invalid');
    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = '';
    };
};

const showReason = function(e) {
    // const target = e.target;
    //const errorEl = inputEl.parentElement.querySelector('.error');
    const reasonFormEl = document.getElementById('reason-form')

    if (e.target.classList.contains('yes-job-opportunity') && 
    (e.target.checked === true)) {   

        // Creates a new <div> element for job title
        const newDivJob = document.createElement('div');
        newDivJob.className += 'form-group option';

        // Creates a new <label> element
        const newLabelJob = document.createElement('label');
        // Sets the inner HTML of the new <label> element
        const newLabelJobTextNode = document.createTextNode("Job Title");
        newLabelJob.setAttribute('for', 'job-title')
        // Creates a new <input> element
        const newInputJob = document.createElement('input');
        // Adds the new <input> element to the containing <div> element
        newInputJob.className +='form-control validate-option';
        newInputJob.setAttribute('id', 'job-title');
        newInputJob.setAttribute('type', 'text');
        newInputJob.setAttribute('name', 'job-title');

        // Adds the new <label> element to the containing <div> element

        newLabelJob.appendChild(newLabelJobTextNode);
        newDivJob.appendChild(newLabelJob);
        newDivJob.appendChild(newInputJob);

        reasonFormEl.appendChild(newDivJob);


        // Creates a new <div> element for website
        const newDivSite = document.createElement('div');
        newDivSite.className += 'form-group option';

        // Creates a new <label> element
        const newLabelSite = document.createElement('label');
        // Sets the inner HTML of the new <label> element
        const newLabelSiteTextNode = document.createTextNode("Website");
        newLabelSite.setAttribute('for', 'website')
        // Creates a new <input> element
        const newInputSite = document.createElement('input');
        // Adds the new <input> element to the containing <div> element
        newInputSite.className += 'form-control validate-option';
        newInputSite.setAttribute('id', 'website');
        newInputSite.setAttribute('type', 'text');
        newInputSite.setAttribute('name', 'website');
        
        // Adds the new <label> element to the containing <div> element

        newLabelSite.appendChild(newLabelSiteTextNode);
        newDivSite.appendChild(newLabelSite);
        newDivSite.appendChild(newInputSite);

        reasonFormEl.appendChild(newDivSite);

    } else if (e.target.classList.contains('yes-talk-code') && (
        e.target.checked === true)) {

        // Creates a new <div> element for code
        const newDivCode = document.createElement('div');
        newDivCode.className += 'form-group option';

        // Creates a new <label> element
        const newLabelCode = document.createElement('label');
        // Sets the inner HTML of the new <label> element
        const newLabelCodeTextNode = document.createTextNode("Coding Language");
        newLabelCode.setAttribute('for', 'coding-language')
        // Creates a new <input> element
        const newInputCode = document.createElement('input');
        // Adds the new <input> element to the containing <div> element
        newInputCode.className += 'form-control validate-option';
        newInputCode.setAttribute('id', 'coding-language');
        newInputCode.setAttribute('type', 'text');
        newInputCode.setAttribute('name', 'coding-language');

        // Adds the new <label> element to the containing <div> element
        newLabelCode.appendChild(newLabelCodeTextNode);
        newDivCode.appendChild(newLabelCode);
        newDivCode.appendChild(newInputCode);
        reasonFormEl.appendChild(newDivCode);
    } else if (e.target.checked === false) {
        if (e.target.classList.contains('yes-job-opportunity')) {
            document.getElementById('job-title').parentNode.remove();
            document.getElementById('website').parentNode.remove();
        } else if (e.target.classList.contains('yes-talk-code')) {
            document.getElementById('coding-language').parentNode.remove();
        };
    };
    
};


const checkEls = document.getElementsByClassName('form-check-input');


const reasonFormEl = document.getElementById('reason-form')
.addEventListener('click', function(e) {
    // for (let i = 0; i < checkEls.length-1; i++) {
    //     showReason(checkEls[i], e);
    // }
            showReason(e);

    })

const storeContactType = function() {
    const contactTypeDivs = document.getElementsByClassName('form-check');
    let contactTypeEls = [];
    for (let i = 0; i < contactTypeDivs.length; i++) {
        if (contactTypeDivs[i].firstChild.nextElementSibling.checked) {
            contactTypeEls[i] = contactTypeDivs[i].firstChild.nextElementSibling.nextElementSibling.innerText;
        }
    }
    //console.log(contactTypeEls)
    localStorage.setItem('contact_type',contactTypeEls);
}
    


const inputEls = document.getElementsByClassName('validate-input');
const formEl = document.getElementById('connect-form')
.addEventListener('submit', function(e) {
    for (let i = 0; i < inputEls.length; i++) {
        if (i < 1) {
            validateName(inputEls[i], e);
        } else if  (i === 1) {
            validateEmail(inputEls[i],e);
        } else if  (i === 2) {
            validateMessage(inputEls[i],e);
        }
        storeContactType();
        //e.preventDefault;
    };
})
