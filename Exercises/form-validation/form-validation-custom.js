/* Validates an individual input on form submitEvent
/* @param {HTMLElement} inputEl
/* @param {Event} sumbitEvent
*/

const validateName = function(inputEl, submitEvent) {
    // const target = submitEvent.target;
    //var constraint = new RegExp(constraints, "");

    const errorEl = inputEl.parentElement.querySelector('.error');
    
    if (inputEl.value === '' || inputEl.value.length < 3) {
        submitEvent.preventDefault();
        console.log('Bad input');

        const labelEL  = inputEl.parentElement.querySelector('label');

        errorEl.innerHTML = `${labelEL.innerText} must be at least 3 characters long.`;
        inputEl.parentElement.classList.add('invalid');

    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = ``;
    };
};

const validateEmail = function(inputEl, submitEvent) {
    //check email
    const errorEl = inputEl.parentElement.querySelector('.error');

    //const constraints = {
        //name : ['/\w{3,}/', 'Name must have 3 characters or more'],
        const emailRegEx = /\w+@\w+\.\w+/i;//, 'Email must have valid format: e.g. example@email.com']
   // };

    if (inputEl.value === '' || !inputEl.value.match(emailRegEx)) {   
        submitEvent.preventDefault(); 
        const error = 'Bad input';  
        console.log(error);

        const labelEL  = inputEl.parentElement.querySelector('label');
        errorEl.innerHTML = `Valid ${labelEL.innerText} required.`;
        inputEl.parentElement.classList.add('invalid');
    } else {
        inputEl.parentElement.classList.remove('invalid');
        errorEl.innerHTML = '';
    };
};


const inputEls = document.getElementsByClassName('validate-input');
const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
        for (let i = 0; i < inputEls.length; i++) {
            if (i < 2) {
                validateName(inputEls[i], e);
            } else {
                validateEmail(inputEls[i],e);
            }
        }

	//e.preventDefault();
});