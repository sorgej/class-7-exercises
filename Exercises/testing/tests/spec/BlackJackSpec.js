//describe("Player", function() {
  //var player;
  //var song;

  // beforeEach(function() {
  //   player = new Player();
  //   song = new Song();
  // });
  describe('calcPoints Test', function() {
    describe('test three hands of two cards each', function() {
      it('calcPoints total score', function() {
        const hand1 = [
          {
            suit: 'spades',
            val: 10,
            displayVal: '10'
          },
          {
            suit: 'spades',
            val: 7,
            displayVal: '7'
          }
        ];
        
        const hand2 = [
          {
            suit: 'spade',
            val: 11,
            displayVal: 'Ace'
          },
          {
            suit: 'spades',
            val: 9,
            displayVal: '9'
          }
        ];
        
        const hand3 = [
          {
            suit: 'spades',
            val: 10,
            displayVal: '10'
          },
          {
            suit: 'spades',
            val: 6,
            displayVal: '6'
          },
          {
            suit: 'spades',
            val: 11,
            displayVal: 'Ace'
          }
        ];
        
        expect(calcPoints(hand1).total).toEqual(17);
        expect(calcPoints(hand2).total).toEqual(20);
        expect(calcPoints(hand3).total).toEqual(17);
        })
    })
  })

